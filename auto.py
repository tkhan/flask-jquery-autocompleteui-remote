import sqlite3
import json
from flask import Flask, request, session, g, jsonify, redirect, url_for, \
     abort, render_template, flash

# configuration
DATABASE = 'C:\\Users\\Buster\\Desktop\\autor\\animals.db'
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    g.db.close()

@app.route('/jsontimes/')
def jsontimes():
    q = request.args.get('term')
    cur =  g.db.execute("SELECT * FROM Animals WHERE name LIKE ?", ('%'+q+'%',))
    entries = cur.fetchall()
    search = []
    for entry in entries:
	    search.append({'id':entry[0], 'value':entry[1], 'label':entry[1]})

    return json.dumps(search)

@app.route('/')
def index():
	return render_template('front.html')

if __name__ == '__main__':
    app.run()
